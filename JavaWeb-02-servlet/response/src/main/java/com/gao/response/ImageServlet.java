package com.gao.response;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author lenovo
 * @Date 2021/8/25 10:31
 */
public class ImageServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 如何让浏览器自动刷新
        resp.setHeader("refresh", "5");

        // 在内存中创建图片
        BufferedImage bufferedImage = new BufferedImage(80, 20, BufferedImage.TYPE_INT_RGB);

        // 得到图片
        Graphics2D graphics2D = (Graphics2D) bufferedImage.getGraphics();

        // 设置图片背景颜色
        graphics2D.setColor(Color.white);
        graphics2D.fillRect(0, 0, 80, 80);

        // 给图片写数据
        graphics2D.setColor(Color.BLUE);
        graphics2D.setFont(new Font(null,Font.BOLD,20));
        graphics2D.drawString(makeNum(),0,20);

        // 告诉浏览器这个请求用图片的方式打开
        resp.setContentType("image/jpeg");

        // 网站存在缓存，不让浏览器缓存
        resp.setHeader("expires","-1");
        resp.setHeader("Cache-Control","no-cache");
        resp.setHeader("Pragma","no-cache");

        // 把图片写给浏览器
        boolean write = ImageIO.write(bufferedImage, ".jpg", resp.getOutputStream());

        resp.sendRedirect("//");
    }

    // 生成随机数
    private String makeNum() {
        Random random = new Random();
        String s = random.nextInt(9999999) + "";
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < 7 - s.length(); i++) {
            sb.append("0");
        }
        String s1 = sb.toString() + s;
        return s;

    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
