package com.gao.response;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLEncoder;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author lenovo
 * @Date 2021/8/25 9:13
 */
public class Response extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String realPath = "G:\\java\\JavaWeb\\JavaWeb-02-servlet\\response\\src\\main\\resources\\图片.png";
        System.out.println("下载路径："+realPath);

        String fileName = realPath.substring(realPath.lastIndexOf("\\") + 1);

        resp.setHeader("Content-Disposition","attachment;filename="+ URLEncoder.encode(fileName,"UTF-8"));

        FileInputStream in = new FileInputStream(realPath);
        int len = 0;
        byte[] buffer = new byte[1024];

        ServletOutputStream os = resp.getOutputStream();
        while((len=in.read(buffer))>0){
            os.write(buffer,0,len);
        }
        in.close();
        os.close();
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
